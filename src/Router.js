/**
 * Created by mario on 13/03/17.
 */
import React from 'react';
import {Scene, Router} from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import InviteeList from './components/InviteeList';
import { Icon } from '@shoutem/ui';

import { logoutUser } from './actions';

const RouterComponent = () => {
    return (
        <Router
            navigationBarStyle={styles.navigationBarStyle}
            titleStyle={styles.titleStyle}
            sceneStyle={styles.sceneStyle}>
            <Scene key="auth">
                <Scene key="login" component={LoginForm} title="T&D: Acceso de Invitado"
                       initial/>
            </Scene>
            <Scene key="main">
                <Scene key="inviteeList" component={InviteeList} title="T&D: Confirmar Asistencia"
                       rightTitle={<Icon name="close" />}
                       onRight={() => { console.log('saliendo'); logoutUser(); } }/>
            </Scene>
        </Router>
    );
};
const styles = {
    navigationBarStyle: {
        backgroundColor: '#008B9F',
        justifyContent: 'center',
        alignItems: 'flex-start',
        // height: 60,
        paddingLeft: 15,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        elevation: 2,
    },
    sceneStyle: {
        paddingTop: 65
    },
    titleStyle: {
        fontSize: 20,
        color: '#fff',
        fontWeight: '600'
    }
};

export default RouterComponent;