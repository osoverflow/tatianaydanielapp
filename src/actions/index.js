/**
 * Created by mario on 7/03/17.
 */
import SailsPromise from 'sails-promise';
import {Actions} from 'react-native-router-flux';
import md5 from 'md5';

// const ROOT_URL = 'https://tatsjuda.apps.opticalcube.com';
// SailsPromise.config({url:ROOT_URL});

import * as types from './types';

export function fetchInvitees() {
    const request = SailsPromise.get('/invitee', {});
    return {
        type: types.FETCH_INVITEES,
        payload: request
    };
}

export const updateInvitee = ({id, confirmed}) => {
    return (dispatch) => {
        SailsPromise.put('/invitee/' + id, {confirmed})
            .then(invitee => {
                dispatch({
                    type: types.UPDATE_INVITEE,
                    payload: invitee
                });
            })
            .catch((err) => {
                dispatch({
                    type: types.ACCESS_DENIED,
                    payload: err
                });
            });
    }
}

export const isAuthenticated = () => {
    return (dispatch) => {
        SailsPromise.get('/user/me')
            .then(user => {
                dispatch({
                    type: types.AUTHENTICATED,
                    payload: user
                });
                Actions.main();
                return null;
            })
            .catch((err) => {
                dispatch({
                    type: types.NOT_AUTHENTICATED,
                    payload: err
                });
            });
    }
}

export const logoutUser = () => {
    const request = SailsPromise.get('/logout');
    request.then(() => {
        Actions.auth();
        return null;
    });
    return {
        type: types.LOGGED_OUT
    };
}

export const loginUser = ({identifier, password}) => {
    return (dispatch) => {
        dispatch({type: types.LOGIN_USER});
        if (password.length < 3) password = md5(password);
        SailsPromise.auth().signInWithIdentifierAndPassword(identifier, password)
            .then(user => {
                dispatch({
                    type: types.LOGIN_USER_SUCCESS,
                    payload: user
                });
                Actions.main();
                return null;
            })
            .catch((err) => {
                dispatch({
                    type: types.LOGIN_USER_FAILED,
                    payload: err
                });
            });
    };
}

export const loginFieldChanged = ({prop, value}) => {
    return {
        type: types.LOGIN_FIELD_CHANGED,
        payload: {prop, value}
    }
}