/**
 * Created by mario on 7/03/17.
 */
import React, {Component} from 'react';
import {View, Text, Linking, ScrollView, ListView, Keyboard} from 'react-native';
import {connect} from 'react-redux';

import {fetchInvitees, updateInvitee} from '../actions/index';

import Invitee from './Invitee';

import {Spinner, Button, Card, CardSection} from './common';

class InviteeList extends Component {
    state = {loading: false};

    componentWillMount() {
        Keyboard.dismiss();
        this.setState({loading: true});
        this.createDatasource(this.props);
        this.props.fetchInvitees();
    }

    componentWillReceiveProps(nextProps) {
        this.createDatasource(nextProps);
        this.setState({loading: false});
    }

    createDatasource(props) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(props.invitees);
    }

    onCheckedChange(e, invitee) {
        this.props.updateInvitee({id: invitee, confirmed: e.checked});
    }

    renderRow(invitee) {
        return (
            <Invitee key={invitee.id} onCheckedChange={this.onCheckedChange.bind(this)} invitee={invitee}/>
        );
    }

    render() {
        if (this.state.loading)
            return <Spinner />
        return (
            <Card>
                <CardSection>
                    <Button onPress={() => Linking.openURL('http://tiny.cc/tatianaydaniel')}>Mas Información</Button>
                </CardSection>

                <ListView
                    enableEmptySections
                    dataSource={this.dataSource}
                    renderRow={this.renderRow.bind(this)}
                />
            </Card>
        )
    }
}

const styles = {};

const mapStateToProps = (state) => {
    return {invitees: state.invitees.all};
}

export default connect(mapStateToProps, {fetchInvitees, updateInvitee})(InviteeList);

