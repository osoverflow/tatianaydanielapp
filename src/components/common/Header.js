/**
 * Created by mario on 6/03/17.
 */
import React from 'react';
import {Text, View} from 'react-native';

const Header = (props) => {
    const {textStyle, viewStyle} = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.children}</Text>
        </View>
    );
}

const styles = {
    viewStyle: {
        backgroundColor: '#008B9F',
        justifyContent: 'center',
        alignItems: 'flex-start',
        height: 40,
        paddingLeft: 15,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    },
    textStyle: {
        fontSize: 20,
        color: '#fff'
    }
};

export { Header };