/**
 * Created by mario on 7/03/17.
 */
import React, {Component} from 'react';
import {TextInput, Text, Keyboard} from 'react-native';
import {connect} from 'react-redux';


import {loginUser, loginFieldChanged, isAuthenticated} from '../actions/index';

import {Card, CardSection, Button, Input, Spinner} from './common';

class LoginForm extends Component {
    state = {identifier: '', password: ''};

    componentWillMount() {
        this.props.isAuthenticated();
    }

    onButtonPress() {
        Keyboard.dismiss();
        this.props.loginUser(this.props.auth);
    }

    renderButton() {
        if (this.props.auth.loading)
            return <Spinner size="small"/>

        return <Button onPress={this.onButtonPress.bind(this)}>Log in</Button>;
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <Input
                        label="Nombre de invitación"
                        placeholder="invitación"
                        value={this.props.auth.identifier}
                        onChangeText={text => this.props.loginFieldChanged({prop: 'identifier', value: text})}
                        autoCorrect={false}
                    />
                </CardSection>
                <CardSection>
                    <Input
                        label="Número de Invitados"
                        placeholder="# invitados"
                        secureTextEntry={false}
                        value={this.props.auth.password}
                        onChangeText={text => this.props.loginFieldChanged({prop: 'password', value: text})}
                    />
                </CardSection>
                <Text style={styles.errorTextStyle}>{this.props.auth.error}</Text>
                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        );
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red'
    }
}

const mapStateToProps = (state) => {
    return {auth: state.auth};
}

export default connect(mapStateToProps, {loginUser, loginFieldChanged, isAuthenticated})(LoginForm);
