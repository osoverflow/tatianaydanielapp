/**
 * Created by mario on 7/03/17.
 */
import React, {Component} from 'react';
import {Text, View, Image, Linking, TouchableOpacity, LayoutAnimation} from 'react-native';
import {MKSwitch} from 'react-native-material-kit';

import {Button} from './common';


class Invitee extends Component {
    componentWillUpdate() {
        LayoutAnimation.spring();
    }

    render() {
        if (!this.props.invitee) return <Text></Text>;
        const {props} = this;

        const image = "http://lorempixel.com/50/50/people";
        // const image = "https://i.imgur.com/K3KJ3w4h.jpg";
        return (
            <View style={styles.containerStyle}>
                {/*
                 <View style={styles.imageStyle}>
                 <Image style={styles.thumbnailStyle} source={{uri: image}}/>
                 </View>
                 */}
                <View style={styles.contentStyle}>
                    <View>
                        <Text style={styles.textStyle}>{props.invitee.name}</Text>
                        <Text>Mesa {props.invitee.seat ? props.invitee.seat : 'No Asignada'}</Text>
                    </View>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <MKSwitch
                            onColor="rgba(0,122,255,.3)"
                            thumbOnColor="rgba(0,122,255,.3)"
                            rippleColor="rgba(255,152,0,.2)"
                            onCheckedChange={(e) => props.onCheckedChange(e, props.invitee.id)}
                            checked={this.props.invitee.confirmed}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    containerStyle: {
        borderWidth: 1,
        borderRadius: 2,
        borderColor: '#ddd',
        borderBottomWidth: 0,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flex: 1
    },
    imageStyle: {
        marginRight: 5
    },
    thumbnailStyle: {
        width: 50,
        height: 50,
        flex: 1,
    },
    contentStyle: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    textStyle: {
        fontSize: 17,
        color: '#111',
        fontWeight: '600'
    }
};

export default Invitee;