/**
 * Created by mario on 7/03/17.
 */
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import SailsPromise from 'sails-promise';


// const ROOT_URL = 'https://tatianaydaniel.herokuapp.com';
const ROOT_URL = 'https://tatsjuda.opticalcube.com';
SailsPromise.config({url:ROOT_URL});

import reducers from './reducers';

import Router from './Router';

const middleware = [ promise, thunk ];
const store = createStore(reducers, {}, applyMiddleware(...middleware));

export default class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Router/>
            </Provider>
        );
    }
}
