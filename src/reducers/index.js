/**
 * Created by mario on 7/03/17.
 */
import {combineReducers} from 'redux';

import InviteesReducer from './reducer_invitees';
import AuthenticateReducer from './reducer_authenticate';

const rootReducer = combineReducers({
  invitees: InviteesReducer,
  auth: AuthenticateReducer,
});

export default rootReducer;
