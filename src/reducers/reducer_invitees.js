import * as types from '../actions/types';

const INITIAL_STATE = {
    all: [],
    invitee: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.FETCH_INVITEES:
            return {...state, all: action.payload.data};
        case types.FETCH_INVITEE:
            return {...state, invitee: action.payload.data};
        case types.UPDATE_INVITEE:
            return {...state, invitee: action.payload.data};
        default:
            return state;
    }
}
