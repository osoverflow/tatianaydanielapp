/**
 * Created by mario on 10/03/17.
 */
import * as types from '../actions/types';

const INITIAL_STATE = {
    user: null,
    identifier: '',
    password: '',
    loading: false,
    error: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.LOGGED_OUT:
            return {...state, ...INITIAL_STATE };
        case types.LOGIN_FIELD_CHANGED:
            return {...state, [action.payload.prop]: action.payload.value };
        case types.AUTHENTICATED:
            return {...state, user: action.payload.data};
        case types.NOT_AUTHENTICATED:
            return {...state, ...INITIAL_STATE };
        case types.LOGIN_USER:
            return {...state, error: '', loading: true};
        case types.LOGIN_USER_SUCCESS:
            return {...state, ...INITIAL_STATE, user: action.payload};
        case types.LOGIN_USER_FAILED:
            return {...state, user: null, loading: false, password: '', error: 'Authentication Failed'};
        default:
            return state;
    }
}
